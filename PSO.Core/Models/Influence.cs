﻿using System;
namespace PSO.Core.Models
{
    public class Influence
    {
        public double Momentum { get; set; }
        public double Personal { get; set; }
        public double Social { get; set; }

        public Influence() : this(1,2,2) { }
        public Influence(double momentum, double personal, double social)
        {
            Momentum = momentum;
            Personal = personal;
            Social = social;
        }
    }
}
