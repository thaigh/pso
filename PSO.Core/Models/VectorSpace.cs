﻿using System;
using System.Collections.Generic;

namespace PSO.Core.Models
{
    public class VectorSpace : IVectorSpace
    {
        public List<NVector> Position { get; set; }
        public List<NVector> Velocity { get; set; }
        public readonly int DimensionSpace;

        private static Random Rand = new Random ();

        public VectorSpace (int dimensionSize)
        {
            Position = new List<NVector> ();
            Velocity = new List<NVector>();
            DimensionSpace = dimensionSize;
        }

        public void AddVector(NVector position, NVector velocity)
        {
            if (position.Length != velocity.Length)
                throw new ArgumentException ("Position Vector length must match Velocity Vector length", nameof (position));

            if (position.Length != DimensionSpace)
                throw new ArgumentException ("Position/Velocity Vector lengths must match dimension space", nameof(position));

            Position.Add (position);
            Velocity.Add (velocity);
        }

        public void UpdateVelocity(Influence influence, NVector personalBest, NVector globalBest)
        {
            for (int i = 0; i < Velocity.Count; i++) {
                NVector newVelocity = CalculateNewVelocity (Velocity[i], Position[i], influence, personalBest, globalBest);
                Velocity[i] = newVelocity;
            }
        }

        private NVector CalculateNewVelocity(
            NVector velocity, NVector position,
            Influence influence,
            NVector personalBest,
            NVector globalBest)
        {
            // Update the particle's velocity:
            //     v_current = (cp1 * v_current) + ((cp2  * rand1) * (p_best - p_current)) + ((cp3 * rand2) * (p_global - p_current))
            //     v[] = (v[]) + (c1 * rand() * (pbest[] - present[])) + (c2 * rand() * (gbest[] - present[]))

            double personalChange = Rand.NextDouble ();
            double socialChange   = Rand.NextDouble ();

            NVector personalBestDiff = personalBest - position;
            NVector globalBestDiff = globalBest - position;

            NVector updatedVelocity =
                (velocity * influence.Momentum) +
                (personalBestDiff * (influence.Personal * personalChange)) +
                (globalBestDiff * (influence.Social * socialChange));

            return updatedVelocity;
        }

        public void UpdatePosition()
        {
            for (int i = 0; i < Position.Count; i++)
            {
                Position [i] += Velocity [i];
            }
        }
    }

    public interface IVectorSpace
    {
        void UpdateVelocity (Influence influence, NVector personalBest, NVector globalBest);
        void UpdatePosition ();
    }
}
