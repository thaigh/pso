﻿using System;
namespace PSO.Core.Models
{
    public class SwarmTargetDetails
    {
        public double Target { get; set; }
        public double ErrorMarginForSuccess { get; set; }

        public SwarmTargetDetails(double target) : this(target, 0) { }
        public SwarmTargetDetails(double target, double errorMargin)
        {
            this.Target = target;
            this.ErrorMarginForSuccess = errorMargin;
        }
    }
}
