﻿using System;
using RP.Math;

namespace PSO.Core.Models
{
    public interface IParticle
    {
        double EvaluateBestFitness ();
        double EvaluateCurrentFitness ();

        /// <summary>
        /// Initialize the particle's position with a uniformly
        /// distributed random vector in range [lowerBound, upperBound]
        /// </summary>
        void InitialisePosition (double lowerBound, double upperBound);

        /// <summary>
        /// Initialize the particle's velocity: v_current
        /// in range [-Abs(upperBound - lowerBound), Abs(upperBound - lowerBound)]
        /// </summary>
        /// <param name="lowerBound">Lower bound.</param>
        /// <param name="upperBound">Upper bound.</param>
        void InitialiseVelocity (double lowerBound, double upperBound);

        NVector PersonalBestPosition { get; set; }
        NVector CurrentPosition { get; set; }
        NVector CurrentVelocity { get; set; }
        //Dimension Dimension { get; set; }
    }
}
