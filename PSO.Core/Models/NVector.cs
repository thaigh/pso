﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace PSO.Core.Models
{
    public class NVector : IEnumerable<double>
    {
        private double [] _data;

        public double X { get { return _data [0]; } }
        public double Y { get { return _data [1]; } }
        public double Z { get { return _data [2]; } }
        public double W { get { return _data [3]; } }
        public double U { get { return _data [4]; } }
        public double V { get { return _data [5]; } }

        public NVector(int dimensionSpace) { _data = new double [dimensionSpace]; }

        public NVector (double x) { _data = new double [] { x }; }
        public NVector (double x, double y) { _data = new double [] { x, y }; }
        public NVector (double x, double y, double z) { _data = new double [] { x, y, z }; }
        public NVector (double x, double y, double z, double w) { _data = new double [] { x, y, z, w }; }
        public NVector (double x, double y, double z, double w, double u) { _data = new double [] { x, y, z, w, u }; }
        public NVector (double x, double y, double z, double w, double u, double v) { _data = new double [] { x, y, z, w, u, v }; }

        public NVector(double[] data) { _data = data; }

        public double this [int key] {
            get { return _data [key]; }
            private set { _data [key] = value; }
        }

        public int Length { get { return _data.Length; } }

        public NVector Add (NVector b) { return Add (this, b); }
        public NVector Subtract (NVector b) { return Subtract (this, b); }
        public NVector MultiplyScalar (double scalar) { return MultiplyScalar (this, scalar); }
        public NVector Clamp (double lowerBound, double upperBound) { return Clamp (this, lowerBound, upperBound); }
        public NVector Wrap  (double lowerBound, double upperBound) { return Wrap  (this, lowerBound, upperBound); }
        public double EuclideanDistance(NVector other) { return EuclideanDistance (this, other); }

        public double SumComponents()
        {
            return _data.Sum (d => d);
        }

        public double MultipleComponents ()
        {
            double sum = Double.NaN;
            foreach (var d in _data) { sum = (double.IsNaN(sum)) ? d : sum * d; }
            return sum;
        }

        public static NVector Add(NVector a, NVector b)
        {
            double [] add = a._data.Add (b._data);
            NVector vec = new NVector (add);
            return vec;
        }

        public static NVector Subtract (NVector a, NVector b)
        {
            double [] sub = a._data.Subtract (b._data);
            NVector vec = new NVector (sub);
            return vec;
        }

        public static NVector MultiplyScalar (NVector a, double scalar)
        {
            double [] mult = a._data.MultiplyScalar (scalar);
            NVector vec = new NVector (mult);
            return vec;
        }

        public static NVector operator + (NVector a, NVector b) { return Add (a, b); }
        public static NVector operator - (NVector a, NVector b) { return Subtract (a, b); }
        public static NVector operator * (NVector a, double scalar) { return MultiplyScalar (a, scalar); }

        public static NVector RandomVector(int numberOfDimensions, double lowerBound, double upperBound)
        {
            Random rand = new Random ();

            double [] data = new double [numberOfDimensions];
            for (int i = 0; i < data.Length; i++)
            {
                data[i] = rand.NextDouble (lowerBound, upperBound);
            }

            return new NVector (data);
        }

        public static NVector Clamp (NVector vec, double lowerBound, double upperBound)
        {
            NVector res = new NVector (vec._data);

            for (var i = 0; i < res.Length; i++) {
                if (res [i] < lowerBound) res [i] = lowerBound;
                if (res [i] > upperBound) res [i] = upperBound;
            }

            return res;
        }

        public static NVector Wrap (NVector vec, double lowerBound, double upperBound)
        {
            NVector res = new NVector (vec._data);

            for (var i = 0; i < res.Length; i++) {
                if (res [i] < lowerBound) {
                    double diff = res [i] + lowerBound;
                    res [i] = upperBound - diff;
                }
                if (res [i] > upperBound) {
                    double diff = res [i] - upperBound;
                    res [i] = lowerBound + diff;
                }
            }

            return res;
        }

        public static double EuclideanDistance(NVector a, NVector b)
        {
            return Math.Sqrt (SquaredLength(a, b));
        }

        private static double SquaredLength(NVector a, NVector b)
        {
            if (a.Length != b.Length)
                throw new ArgumentException ("Vector lengths to not match");

            double squaredSum = 0;
            for (int i = 0; i < a.Length; i++) {
                double diff = Math.Pow (a [i] - b [i], 2);
                squaredSum += diff;
            }

            return squaredSum;
        }

        public IEnumerator<double> GetEnumerator ()
        {
            for (int i = 0; i < Length; i++)
                yield return _data [i];
        }

        IEnumerator IEnumerable.GetEnumerator ()
        {
            return GetEnumerator ();
        }
    }
}
