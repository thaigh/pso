﻿using System;
namespace PSO.AddToValue.Models
{
    public class Range
    {
        public double Lower { get; set; }
        public double Upper { get; set; }

        public Range(double lower, double upper) {
            Set (lower, upper);
        }

        public void SetLower(double lower) {
            CheckBounds (lower, this.Upper);
            this.Lower = lower;
        }

        public void SetUpper(double upper) {
            CheckBounds (this.Lower, upper);
            this.Upper = upper;
        }

        public void Set(double lower, double upper) {
            CheckBounds (lower, upper);
            this.Lower = lower;
            this.Upper = upper;
        }

        private void CheckBounds(double lower, double upper) {
            if (lower > upper)
                throw new ArgumentException ("lower should be less than or equal to upper");
        }
    }
}
