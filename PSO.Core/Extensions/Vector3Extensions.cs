﻿using System;

namespace RP.Math
{
    public static class Vector3Extensions
    {
        public static Vector3 RandomVector(double lowerBound, double upperBound)
        {
            Random rand = new Random ();
            double x = rand.NextDouble (lowerBound, upperBound);
            double y = rand.NextDouble (lowerBound, upperBound);
            double z = rand.NextDouble (lowerBound, upperBound);
            return new Vector3 (x, y, z);
        }
    }
}
