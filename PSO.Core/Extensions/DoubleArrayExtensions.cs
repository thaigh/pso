﻿
namespace System
{
    public static class DoubleArrayExtensions
    {
        public static double[] Add(this double [] self, double[] otherArray)
        {
            if (self.Length != otherArray.Length)
                throw new Exception ("Array lengths must be equal");

            double [] res = new double [self.Length];
            for (int i = 0; i < self.Length; i++)
            {
                res [i] = self [i] + otherArray [i];
            }

            return res;
        }

        public static double [] Subtract (this double [] self, double [] otherArray)
        {
            if (self.Length != otherArray.Length)
                throw new Exception ("Array lengths must be equal");

            double [] res = new double [self.Length];
            for (int i = 0; i < self.Length; i++) {
                res [i] = self [i] - otherArray [i];
            }

            return res;
        }

        public static double[] MultiplyScalar(this double [] self, double scalar)
        {
            var res = new double [self.Length];
            for (int i = 0; i < self.Length; i++) {
                res [i] = self [i] * scalar;
            }
            return res;
        }

        public static double [] MultiplyScalar (this double scalar, double [] array)
        {
            var res = new double [array.Length];
            for (int i = 0; i < array.Length; i++) {
                res [i] = array [i] * scalar;
            }
            return res;
        }

    }
}
