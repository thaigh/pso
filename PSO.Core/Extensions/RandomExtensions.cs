﻿using System;

namespace System
{
    /// <summary>
    /// Extensions for System.Random class. Does not contain random extensions
    /// </summary>
    public static class RandomExtensions
    {
        public static double NextDouble(this Random rand, double lower, double upper)
        {
            return lower + (rand.NextDouble () * upper);
        }
    }
}
