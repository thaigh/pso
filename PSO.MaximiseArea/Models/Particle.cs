﻿using System;
using PSO.Core.Models;
using RP.Math;

namespace PSO.MaximiseArea.Models
{
    public class Particle : IParticle
    {
        private NVector _currentPosUnconstrained;
        public NVector CurrentPosition {
            get { return _currentPosUnconstrained; }
            set
            {
                _currentPosUnconstrained = value;
                ConstrainedCurrentPosition = ConstrainPosition (value);
            }
        }

        public NVector CurrentVelocity { get; set; }

        private NVector _personalBestPosition;
        public NVector PersonalBestPosition {
            get { return _personalBestPosition; }
            set
            {
                _personalBestPosition = value;
                ConstrainedPersonalBestPosition = ConstrainPosition (value);
            }
        }

        public NVector ConstrainedCurrentPosition { get; set; }
        public NVector ConstrainedPersonalBestPosition { get; set; }

        const int DimensionSpace = 1;
        const int ConstrainedDimensionSpace = 3;

        public Particle()
        {
            CurrentPosition = new NVector (DimensionSpace);
            CurrentVelocity = new NVector (DimensionSpace);
            PersonalBestPosition = CurrentPosition;

            ConstrainedCurrentPosition = new NVector (ConstrainedDimensionSpace);
            ConstrainedPersonalBestPosition = new NVector (ConstrainedDimensionSpace);
        }

        public double EvaluateBestFitness ()
        {
            //return PersonalBestPosition.X * PersonalBestPosition.Y;
            return ConstrainedPersonalBestPosition.X * ConstrainedPersonalBestPosition.Y;
        }

        public double EvaluateCurrentFitness ()
        {
            //return CurrentPosition.X * CurrentPosition.Y;
            return ConstrainedCurrentPosition.X * ConstrainedCurrentPosition.Y;
        }

        public NVector ConstrainPosition(NVector pos)
        {
            // Constrained for:
            // 500 = x + 2y
            // Solving for x:
            // x = 500 - 2y
            // Solving for y:
            // y = (500 - x) / 2

            var y = pos.X;
            var z = y;
            var x = 500 - 2 * y;
            NVector constrainedPos = new NVector (x, y, z);
            return constrainedPos;
        }

        public void InitialisePosition (double lowerBound, double upperBound)
        {
            CurrentPosition = NVector.RandomVector (DimensionSpace, lowerBound, upperBound);
            PersonalBestPosition = CurrentPosition;
        }

        public void InitialiseVelocity (double lowerBound, double upperBound)
        {
            CurrentVelocity = NVector.RandomVector (DimensionSpace, lowerBound, upperBound);
        }
    }
}
