﻿using System;
using System.Collections.Generic;
using PSO.AddToValue.Models;
using PSO.Core.Models;
using PSO.MaximiseArea.Models;

namespace PSO.MaximiseArea
{
    class MainClass
    {
        public static void Main (string [] args)
        {
            // Given 500m of fence, find the maximum enclosed area for a paddock
            // that has a building on one side 
            //
            // i.e.
            // Given the Constraint: 500 = x + 2y
            //           Maximise:   x * y
            //
            // Solving Constraint for x:
            //   x = 500 - 2y
            // We can determine Maximise function;
            //   A(y) = (500 - 2y) * y
            //        = 500y - 2y^2
            // And solve for y over [0, 250]

            MainClass mc = new MainClass ();
            mc.Simulate ();

        }


        Particle globalBestParticle = new Particle ();
        List<Particle> swarm = new List<Particle> ();

        const int NumberOfParticles = 1000;
        const int NumberOfEpochs = 10000;

        Range SearchSpace = new Range (0, 250);
        Random rand = new Random();
        Influence Influence = new Influence (1, 2, 2);
        SwarmTargetDetails Target = new SwarmTargetDetails (31250, 0.000000000001);


        public void Simulate ()
        {
            InitialiseParticleSwarm ();
            RunSimulation ();
            DisplayResults ();
        }

        private void InitialiseParticleSwarm ()
        {
            globalBestParticle = new Particle ();
            globalBestParticle.CurrentPosition = new NVector (2);

            // Create and initialise Particles
            swarm = new List<Particle> ();
            for (int i = 0; i < NumberOfParticles; i++) {
                Particle p = GenerateParticle ();
                swarm.Add (p);
            }
        }

        private Particle GenerateParticle ()
        {
            Particle p = new Particle ();
            p.InitialisePosition (SearchSpace.Lower, SearchSpace.Upper);

            // Update the swarm's best known position if p_best < g_best
            EvaluateParticleFitnessAgainstGlobal (p);

            return p;
        }

        private void RunSimulation ()
        {
            // Iterate while not ending criterion
            bool foundSolution = false;
            for (int currentTurn = 0; currentTurn < NumberOfEpochs && !foundSolution; currentTurn++) {
                foreach (var p in swarm) {

                    // Update the particle's velocity
                    p.CurrentVelocity = CalculateNewVelocity (p);

                    // Update the particle's position: p_current = p_current + v_current
                    p.CurrentPosition += p.CurrentVelocity;
                    p.CurrentPosition = p.CurrentPosition.Clamp (SearchSpace.Lower, SearchSpace.Upper);

                    // Compare fitness of current particle to personal / global best
                    if (DistanceToTarget(p.EvaluateCurrentFitness ()) < DistanceToTarget(p.EvaluateBestFitness ())) {
                        p.PersonalBestPosition = p.CurrentPosition;
                        foundSolution = foundSolution || EvaluateParticleAgainstGlobalAndTarget (p);
                    }
                }
            }
        }

        private NVector CalculateNewVelocity (Particle p)
        {
            // Update the particle's velocity:
            //     v_current = (cp1 * v_current) + ((cp2  * rand1) * (p_best - p_current)) + ((cp3 * rand2) * (p_global - p_current))
            //     v[] = (v[]) + (c1 * rand() * (pbest[] - present[])) + (c2 * rand() * (gbest[] - present[]))

            double rand1 = rand.NextDouble ();
            double rand2 = rand.NextDouble ();

            var personalBestDiff = p.PersonalBestPosition - p.CurrentPosition;
            var globalBestDiff = globalBestParticle.CurrentPosition - p.CurrentPosition;

            NVector updatedVelocity =
                (p.CurrentVelocity * Influence.Momentum) +
                (personalBestDiff * (Influence.Personal * rand1)) +
                (globalBestDiff * (Influence.Social * rand2));

            return updatedVelocity;
        }

        private bool EvaluateParticleFitnessAgainstGlobal (Particle p)
        {
            if (ParticleIsBetterThanGlobal (p)) {
                globalBestParticle.CurrentPosition = p.CurrentPosition;
                return true;
            }

            return false;
        }

        private bool ParticleIsBetterThanGlobal (Particle p)
        {
            return DistanceToTarget(p.EvaluateBestFitness ()) < DistanceToTarget(globalBestParticle.EvaluateCurrentFitness ());
        }

        private double DistanceToTarget(double fitness)
        {
            return Math.Abs(Target.Target - fitness);
        }

        private bool EvaluateParticleAgainstGlobalAndTarget (Particle p)
        {
            if (EvaluateParticleFitnessAgainstGlobal (p)) {
                // Check if we have found a solution
                return CheckGlobalFitnessAgainstTarget ();
            }

            // Not better than global best. can't be closer to target solution
            return false;
        }

        private bool CheckGlobalFitnessAgainstTarget ()
        {
            return DistanceToTarget(globalBestParticle.EvaluateCurrentFitness ()) < Target.ErrorMarginForSuccess;
        }

        private void DisplayResults ()
        {
            // Simulation is over. Either we reached the target, or ran out of turns
            Console.WriteLine ("Global Best Particle Result = {3} (Dist = {4}): {0}, {1}, {2}",
                               globalBestParticle.ConstrainedCurrentPosition.X,
                               globalBestParticle.ConstrainedCurrentPosition.Y,
                               globalBestParticle.ConstrainedCurrentPosition.Z,
                               globalBestParticle.ConstrainedCurrentPosition.X *
                               globalBestParticle.ConstrainedCurrentPosition.Y,
                               DistanceToTarget(globalBestParticle.EvaluateCurrentFitness ()));

            //foreach (var p in swarm) {
            //    Console.WriteLine ("Particle Result PB = {3}: {0}, {1}, {2}",
            //                       p.PersonalBestPosition.X,
            //                       p.PersonalBestPosition.Y,
            //                       p.PersonalBestPosition.Z,
            //                       p.PersonalBestPosition.SumComponents ());
            //}
        }


    }
}
