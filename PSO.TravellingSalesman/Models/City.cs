﻿using System;
using PSO.Core.Models;

namespace PSO.TravellingSalesman.Models
{
    public class City
    {
        public readonly NVector Location;

        public double X { get { return Location.X; } }
        public double Y { get { return Location.Y; } }

        public City (int x, int y)
        {
            Location = new NVector (x, y);
        }

    }
}
