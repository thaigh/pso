﻿using System;
using System.Collections.Generic;
using PSO.Core.Models;

namespace PSO.TravellingSalesman.Models
{
    public class Particle :IParticle
    {

        private readonly List<City> _mapLocations;
        private readonly int _dimensionSize;

        public Particle(List<City> mapLocations)
        {
            _mapLocations = mapLocations;
            _dimensionSize = mapLocations.Count;

            CurrentPosition = new NVector (_dimensionSize);
            CurrentVelocity = new NVector (_dimensionSize);
            PersonalBestPosition = new NVector (_dimensionSize);
        }

        private NVector _currentPosition;
        public NVector CurrentPosition {
            get { return _currentPosition; }
            set { _currentPosition = FloorComponents(value); }
        }

        public NVector CurrentVelocity { get; set; }

        private NVector _personalBestPosition;
        public NVector PersonalBestPosition {
            get { return _personalBestPosition; }
            set { _personalBestPosition = FloorComponents (value); }
        }

        public double EvaluateBestFitness ()
        {
            return CalculateDistance (PersonalBestPosition);
        }

        public double EvaluateCurrentFitness ()
        {
            return CalculateDistance (CurrentPosition);
        }

        private double CalculateDistance(NVector vec)
        {
            if (!IsValidPosition (vec)) return -1;

            double totalDistanceTravelled = 0;
            for (int i = 0; i < vec.Length - 1; i++)
            {
                var location1 = LookupMapLocation ((int)vec [i]);
                var location2 = LookupMapLocation ((int)vec [i + 1]);
                totalDistanceTravelled += NVector.EuclideanDistance (location1, location2);
            }

            return totalDistanceTravelled;
        }

        private bool IsValidPosition(NVector pos)
        {
            List<int> vals = new List<int> ();
            foreach (var p in pos)
            {
                int val = (int)p;
                if (vals.Contains (val)) return false;
                vals.Add (val);
            }
            return true;
        }

        private NVector LookupMapLocation(int pos)
        {
            return _mapLocations [pos].Location;
        }

        public void InitialisePosition (double lowerBound, double upperBound)
        {
            CurrentPosition = NVector.RandomVector (_dimensionSize, lowerBound, upperBound);
            PersonalBestPosition = CurrentPosition;
        }

        private NVector FloorComponents(NVector vec)
        {
            double [] data = new double [vec.Length];
            for (int i = 0; i < vec.Length; i++) {
                data [i] = (int) (vec[i]);
            }
            NVector newVec = new NVector (data);
            return newVec;
        }

        public void InitialiseVelocity (double lowerBound, double upperBound)
        {
            CurrentVelocity = NVector.RandomVector (_dimensionSize, lowerBound, upperBound);
        }
    }
}
