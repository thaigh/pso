﻿using System;
using PSO.AddToValue.Models;
using PSO.Core.Models;

namespace PSO.AddToValue
{
    public class SwarmEngineBuilder
    {
        private int _numberOfParticles = 1;
        private double _searchSpaceLowerBound = 0;
        private double _searchSpaceUpperBound = 1;
        private int _numberOfEpochs = 1;
        private double _errorMarginForSuccess = 0.1;
        private double _target = 1;

        public SwarmEngineBuilder NumberOfParticles(int numParticles) {
            this._numberOfParticles = numParticles;
            return this;
        }

        public SwarmEngineBuilder SearchSpaceLowerBound(double lowerBound) {
            this._searchSpaceLowerBound = lowerBound;
            return this;
        }

        public SwarmEngineBuilder SearchSpaceUpperBound(double upperBound) {
            this._searchSpaceUpperBound = upperBound;
            return this;
        }

        public SwarmEngineBuilder NumberOfEpochs(int epochs) {
            this._numberOfEpochs = epochs;
            return this;
        }

        public SwarmEngineBuilder ErrorMarginForSuccess(double errorMargin) {
            this._errorMarginForSuccess = errorMargin;
            return this;
        }
        public SwarmEngineBuilder Target(double target) {
            this._target = target;
            return this;
        }

        public SwarmEngine Build()
        {
            return new SwarmEngine (
                new SwarmEngineConfig(_numberOfParticles, _numberOfEpochs),
                new Range(_searchSpaceLowerBound, _searchSpaceUpperBound),
                new SwarmTargetDetails(_target, _errorMarginForSuccess),
                new Influence(1,2,2)
            );
        }
                                                 
    }
}
