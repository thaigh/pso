﻿using System;
using System.Collections.Generic;
using PSO.AddToValue.Models;
using PSO.Core.Models;
using RP.Math;

namespace PSO.AddToValue
{
    class MainClass
    {
        public static void Main (string [] args)
        {
            // Write a PSO which finds 3 numbers that add up to 50
            SwarmEngine eng = new SwarmEngine (
                new SwarmEngineConfig (1000, 10000),
                new Range (7, 10),
                new SwarmTargetDetails (27.3, 0.0000001),
                new Influence (1,1,2)
            );

            eng.Simulate ();
        }

    }
}
