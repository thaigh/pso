﻿using System;
using PSO.Core.Models;
using RP.Math;

namespace PSO.AddToValue.Models
{
    /// <summary>
    /// Represents a candidate solution to the problem
    /// </summary>
    public class Particle : IParticle
    {
        public NVector PersonalBestPosition { get; set; }
        public NVector CurrentPosition { get; set; }
        public NVector CurrentVelocity { get; set; }

        private const int DimensionSpace = 3;
        public Particle()
        {
            PersonalBestPosition = new NVector (DimensionSpace);
            CurrentPosition = new NVector (DimensionSpace);
            CurrentVelocity = new NVector (DimensionSpace);
        }

        public double EvaluateBestFitness ()
        {
            return EvaluateFitnessFromPosition (PersonalBestPosition);
        }

        public double EvaluateCurrentFitness ()
        {
            return EvaluateFitnessFromPosition (CurrentPosition);
        }

        //const double Target = 27.3;
        private double EvaluateFitnessFromPosition(NVector pos)
        {
            // Evaluate the distance from this particle to target.
            // In this scenario, we see how far the sum of this particle's
            // position is to the target value of 50;
            //
            // Thus we try and minimise error margin
            double totalSum = pos.X + pos.Y + pos.Z;
            //return Math.Abs(Target - totalSum);
            return totalSum;
        }

        public void InitialisePosition (double lowerBound, double upperBound)
        {
            CurrentPosition = NVector.RandomVector (DimensionSpace, lowerBound, upperBound);

            // Initialize the particle's best known position to its initial
            // position: p_best = p_current
            PersonalBestPosition = CurrentPosition;
        }

        public void InitialiseVelocity (double lowerBound, double upperBound)
        {
            double absLowerBound = -1 * Math.Abs (upperBound - lowerBound);
            double absUpperBound =      Math.Abs (upperBound - lowerBound);

            CurrentVelocity = NVector.RandomVector (DimensionSpace, absLowerBound, absUpperBound);
        }

    }
}
