﻿using System;
namespace PSO.Core.Models
{
    public class ParticleDimension
    {
        public float Number1 { get; set; }
        public float Number2 { get; set; }
        public float Number3 { get; set; }
    }
}
