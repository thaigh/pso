﻿using System;
namespace PSO.AddToValue.Models
{
    public class SwarmEngineConfig
    {
        public int NumberOfParticles { get; set; }
        public int NumberOfEpochs { get; set; }

        public SwarmEngineConfig(int particles, int epochs)
        {
            this.NumberOfParticles = particles;
            this.NumberOfEpochs = epochs;
        }
    }
}
